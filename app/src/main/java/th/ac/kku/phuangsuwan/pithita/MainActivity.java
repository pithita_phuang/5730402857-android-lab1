package th.ac.kku.phuangsuwan.pithita;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {
    Button buttonSend;
    EditText box;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        box = (EditText) findViewById(R.id.edtText);
        buttonSend = (Button) findViewById(R.id.btnSend);

        buttonSend.setOnClickListener(this);


    }

    @Override
    public void onClick(View view) {
        if(view.getId()== R.id.btnSend){
            Intent intent = new Intent(getApplicationContext(), MyMessage.class);
            intent.putExtra("data_name", box.getText().toString());
            startActivity(intent);
        }
    }
}
