package th.ac.kku.phuangsuwan.pithita;

import android.content.Context;
import android.content.Intent;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.MenuItem;
import android.widget.TextView;

public class MyMessage extends AppCompatActivity {

    android.app.ActionBar actionBar;
    TextView txtMessage;

    public static Context context;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_my_message);

        String text = getIntent().getExtras().getString("data_name");
        context = getApplicationContext();
        txtMessage = (TextView) findViewById(R.id.tv_Message);
        txtMessage.setText(text);


    }


}
